# Udacity Transcript Transformer

Takes a download of a Udacity class transcript zip file and turns it into Markdown lecture notes.

Run as:

```
dotnet run <path to zip>
```

This will output the Markdown lecture notes in a folder alongside the zip file.