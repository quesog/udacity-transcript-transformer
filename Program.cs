﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text.RegularExpressions;

namespace TranscriptTransformer
{
    static class Extensions
    {
        public static string Join(this IEnumerable<string> arr, string separator = " ")
        {
            if (arr.Count() < 1)
                return string.Empty;
            return arr.Aggregate((a, b) => a + separator + b);
        }
    }

    class SrtParser
    {
        private char[] NewLines { get; } = new[] { '\r', '\n' };

        public string RawContents { get; private set; }
        public string Formatted
        {
            get
            {
                return RawContents.Split("\r\n\r\n")
                    .Select(s => s.Split(NewLines, StringSplitOptions.RemoveEmptyEntries)
                                  .Skip(2)
                                  .Join()
                                  .Replace(">>", "\n\n>>"))
                    .Join();
            }
        }
        public SrtParser(Stream contents)
        {
            RawContents = new StreamReader(contents).ReadToEnd();
        }
    }

    class Lesson
    {
        public string Title { get; set; } = "Untitled Lesson";
        public string Content { get; set; }
    }

    class Program
    {
        private static LazyLog log { get; } = new LazyLog();

        static string TwoNewLines { get; } = Environment.NewLine + Environment.NewLine;

        static string LessonName(string fullName) => fullName.Substring(0, fullName.LastIndexOf('/'));

        static string RemoveBadFileChars(string s) => Regex.Replace(s, $"[{Regex.Escape(new string(Path.GetInvalidFileNameChars()))}]", "");

        // https://stackoverflow.com/a/5093939
        static string PadNumbers(string input) => Regex.Replace(input, "[0-9]+", match => match.Value.PadLeft(10, '0'));

        static void Main(string[] args)
        {
            if (args.Length != 1 || !File.Exists(args[0]))
            {
                log.Error("Invalid Arguments");
                return;
            }
            string fn = args[0];


            try
            {
                var lessons = new List<Lesson>();

                // Read the lessons and parse them 
                var zipFile = ZipFile.OpenRead(fn);
                foreach (var lesson in zipFile.Entries
                                              .GroupBy(entry => LessonName(entry.FullName))
                                              .OrderBy(kv => PadNumbers(kv.Key)))
                {
                    log.Info(lesson.Key);
                    var l = new Lesson() { Title = lesson.Key };
                    l.Content = $"# {lesson.Key}{TwoNewLines}";

                    foreach (var entry in lesson.OrderBy(e => PadNumbers(e.FullName)))
                    {
                        log.Info(entry.FullName);
                        l.Content += $"## {Path.GetFileNameWithoutExtension(entry.FullName)}{TwoNewLines}";
                        using (var stream = entry.Open())
                        {
                            var parser = new SrtParser(stream);
                            l.Content += parser.Formatted + TwoNewLines;
                        }
                    }
                    lessons.Add(l);
                }

                var outPath = Path.Combine(
                    Path.GetDirectoryName(fn),
                    Path.GetFileNameWithoutExtension(fn));
                log.Info($"Writing transcripts to {outPath}");
                Directory.CreateDirectory(outPath);
                foreach (var lesson in lessons)
                {
                    var lessonPath = Path.Combine(outPath, $"{RemoveBadFileChars(lesson.Title)}.md");
                    log.Info($"Writing {lessonPath}...");
                    File.WriteAllText(lessonPath, lesson.Content);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error during conversion", ex);
            }
        }
    }

    class LazyLog
    {
        public void Info(string message)
        {
            Console.WriteLine($"INFO: {message}");
        }
        public void Error(string message, Exception ex = null)
        {
            Console.WriteLine($"ERROR: {message}{(ex != null ? Environment.NewLine : "")}{ex}");
        }
    }
}
